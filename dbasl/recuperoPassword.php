<?php
	require 'connessione_db.php';

	$errore=1; //variabile di controllo errori (se rimane a 0 non ci sono errori)

	if(isset($_POST['eMail']))
    {
   		if($stmt=$link->prepare("SELECT docenti.username, docenti.password FROM docenti WHERE docenti.email='".$_POST['eMail']."';"))
		{
        	$stmt->execute();
            $result=$stmt->get_result();
			//l’hash ci servirà per recuperare i dati utente e confermare la richiesta
			//la password nel database si presume criptata, con md5 o altro algoritmo
			//al posto di questi due dati, se ne possono usare altri legati all’utente, purché univoci
            if ($result->num_rows > 0) {
            	$errore=0;
            	while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
            		$hash=$row['password']."".$row['username'];
				}                   
			}
			$stmt->close();
            else 
            {
            	$errore=1;
			}	
        }
        if($errore==0)
        {
			$header= "From: DB ASL \n";
			$header .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
			$header .= "Content-Transfer-Encoding: 7bit\n\n";
			$subject= "DB ASL - Richiesta cambio password utente";
			$mess_invio="<html><body>";
			$mess_invio.=" Clicca sul <a href=\"http://www.bianchif.it/dbasl/nuovaPassword.php?hash=".$hash."\">link</a> per confermare la nuova password.<br /> Se il link non è visibile, copia la riga qui sotto e incollala sul tuo browser: <br /> http://www.bianchif.it/dbasl/nuovaPassword.php?hash=".$hash." ";
			$mess_invio.='</body><html>';
            //invio email
			if(@mail($_POST['eMail'], $subject, $mess_invio, $header)){
				$str = "<h3>L'eMail per il recupero è stata inviata all'indirizzo inserito!</h3><br><h5>Controlla la casella di posta!</h5><br/><br/>";
				unset($_POST); //elimino le variabili post, in modo che non appaiano nel form
			}
		}
        else if($errore==1)
        {
        	$str='<h3>Errore!</h3> <br> <h5>eMail non inserita o non registrata!</h5>';
        }
	}
    
?>
<html>
	<head>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<title>Password Dimenticata | DB ASL</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/starter-template.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
			<a class="navbar-brand" href="#">DB ASL</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
					</li>
				</ul>
			</div>
		</nav>
		<div class="container">
          
         	<form name="select" method="POST">
				<label class="form-label" placeholder="eMail" for="eMail">Inserisci eMail</label><input type="text" class="form-control" name="eMail"/><br>
				<input type="submit" class="btn btn-dark" value="Invio dati"/><br>
			</form> 
            <h5><?php echo $str;?></h5>
		</div>
        <?php include 'footer.php';?>
	</body>
</html>
			
