<?php
	session_start();
	require 'connessione_db.php';
	if(!isset($_SESSION['username']))
	{		
		//Verifico che la sessione sia attiva
		header('Location: ' . 'login.html');//Se non attiva reindirizzo alla pagina di login
	}
?> 


<html>
	<head>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<title>Homepage | DB ASL</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/starter-template.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
			<a class="navbar-brand" href="#">DB ASL</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
					</li>
                    <li class="nav-item">
						<a class="nav-link" href="modificaPassword.php">Modifica Password</a>
					</li>
                    <?php
                    	if($_SESSION['username']=='admin')
                        {
                        	echo '
                   				 <li class="nav-item">
								 <a class="nav-link" href="nuovoDocente.php">Inserisci Nuovo Docente</a>
								 </li>';
                        }
                    ?>
				</ul>
				<form class="form-inline my-2 my-lg-0" action="logout.php">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
				</form>
			</div>
		</nav>
		<div class="container">
			<h1>Benvenuto!</h1>
			<br>
			<br>
			<h3 class="font-weight-light font-italic">Seleziona la tua classe:</h3>
			<br>
			<form name="select" method="POST" action="visualizzaClasse.php">
				<select id="classe" class="custom-select" name="classe" id="classe">
					<?php
						if ($stmt = $link->prepare("SELECT id, codice, indirizzo, a_s FROM classi ORDER BY a_s DESC, codice ASC;")) 
						{ 
							$stmt->execute(); // esegue la query appena creata.
							//$stmt->store_result();
							//$stmt->bind_result($class_id, $nome, $indirizzo); // recupera il risultato della query e lo memorizza nelle relative variabili.
							//$stmt->fetch();
							// estrazione dei risultati
							$result = $stmt->get_result();
							$stmt->close();
							// conteggio dei record
							if ($result->num_rows > 0) {
								while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
									echo '<option value='.$row['id'].'>'.$row['codice'].'&nbsp;'.$row['indirizzo'].'&nbsp;'.$row['a_s'].'</option>';
								}
							}
						}
					?>
				</select>
				<br>
				<br>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit" >Visualizza classe</button>
			</form>
            <?php print date("d/m/y");?>
			<form align=right action="nuovaClasse.php">
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Aggiungi classe</button>
			</form>
		</div>
        <?php include 'footer.php';?>
	</body>
</html>