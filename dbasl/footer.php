<head>
	<link href="css/footer.css"  rel="stylesheet" type="text/css">
</head>
<br><br><br><br><br>
<footer style="margin-top:100px;">
    <div class="footer" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <h3> DB ASL </h3>
                    <ul>
                        <li>Federico Bianchi</li>
                    </ul>
                </div>
                <div class="col-sm-6">
                	<h3>&nbsp; </h3>
                    <ul>
                        <li>PHP HyperText Processor</li>
                        <li>MySQL & MySQLi APIs</li>
                        <li>Bootstrap Responsive Framework</li>
                    </ul>
                </div>
            </div>
            <!--/.row--> 
        </div>
        <!--/.container--> 
    </div>
    <!--/.footer-->
    
    <div class="footer-bottom">
        <div class="container">
            <p class="muted-text"> Copyright © DB ASL 2018</p>
            
        </div>
    </div>
    <!--/.footer-bottom--> 
</footer>