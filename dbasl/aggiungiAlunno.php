<?php	
	session_start();
	require 'connessione_db.php';
	if(!isset($_SESSION['username']))
	{		
		//Verifico che la sessione sia attiva
		header('Location: ' . 'login.html');//Se non attiva reindirizzo alla pagina di login
	}
    $classe=$_POST['classe'];
	if((isset($_POST["nme"]))and (isset($_POST["cognome"])))
	{				
		$exists=0;
		$nme=strtoupper($_POST["nme"]);
		$surname=strtoupper($_POST["cognome"]);
		if($stmt=$link->prepare('select nome, cognome from alunni where classe="'.$classe.'" and cognome="'.$surname.'" and nome="'.$nme.'";'))
		{
			$stmt->execute();
			$result = $stmt->get_result();
			// conteggio dei record
			if ($result->num_rows == 1) {
				$exists=1;
			}
			$stmt->close();
		}
		if($exists==1)
		{
			echo 'Alunno: '.$_POST['nme'].'&nbsp;'.$_POST['cognome'].' già esistente'; 
		}
		if($exists==0)
		{
			if($stmt=$link->prepare('INSERT INTO alunni (nome, cognome, classe)'.' VALUES ("'.$nme.'", "'.$surname.'","'.$classe.'");'))
			{
				$stmt->execute();
				$stmt->close();
                header ('Location: '.'visualizzaClasse.php?classe='.$classe.'');
			}
		}
	}
?>


<html>
	<head>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<title>Aggiungi Alunno | DB ASL</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/starter-template.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
			<a class="navbar-brand" href="#">DB ASL</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
					    <a class="nav-link" href="home.php">Home</a>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0" action="logout.php">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
				</form>
			</div>
		</nav>
		<div class="container">
			<?php
				if ($stmt = $link->prepare('SELECT codice, indirizzo, a_s FROM classi where id='.$classe.';')) 
				{ 
					$stmt->execute(); // esegue la query appena creata.
					// estrazione dei risultati
					$result = $stmt->get_result();
					// conteggio dei record
					if ($result->num_rows > 0) {
						while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
							echo '<h3 class="font-weight-light font-italic">'.$row['codice'].'&nbsp;'.$row['indirizzo'].'&nbsp;'.$row['a_s'].'</h3><br>';
						}
					}
					$stmt->close();
				}
			?>
			<form name="anagrafica" method="POST">
				 <div class="form-group">
					<label for="nme">Nome:</label>
					<input type="text" maxlength=32 class="form-control" name="nme">
				</div>
				<div class="form-group">
					<label for="cognome">Cognome:</label>
					<input type="text" maxlength=32 class="form-control" name="cognome">
				</div>
				
				<input type="hidden" name="classe" value="<?php echo $classe;?>"/>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Aggiungi</button>
			</form>
            <form action="visualizzaClasse.php" method="POST">
              <input type="hidden" name="classe" value="<?php echo $classe; ?>"/>
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Torna alla classe</button>
          </form>
		</div>
        <?php include 'footer.php';?>
	</body>
</html>
