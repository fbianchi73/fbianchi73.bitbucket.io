<html>
	<head>
	
	</head>
	<body>
	<?php
		require 'connessione_db.php';
		$username=$_POST['utente'];
		$password=hash('sha256',$_POST['pwd']);
		
		// Usando statement sql 'prepared' non sarà possibile attuare un attacco di tipo SQL injection.
		if ($stmt = $link->prepare("SELECT id, username, password FROM docenti WHERE username='".$username."';")) 
		{ 
			$stmt->execute(); // esegue la query appena creata.
			$stmt->store_result();
			$stmt->bind_result($user_id, $username, $db_password); // recupera il risultato della query e lo memorizza nelle relative variabili.
			$stmt->fetch();
			if($stmt->num_rows == 1) 
			{ 
				// se l'utente esiste
				if($db_password == $password) 
				{ 
					// Verifico che la password memorizzata nel database corrisponda alla password fornita dall'utente.
					// Password corretta!            
					$user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro 'user-agent' relativo all'utente corrente.
					session_start();
					$_SESSION['user_id'] = $user_id; 
					// $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username); // ci proteggiamo da un attacco XSS
					$_SESSION['username'] = $username;
					// $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
					// Login eseguito con successo.
					header ('Location: '. 'home.php');
					$now = date('r');
					$link->query("INSERT INTO login_riusciti (user_id, orario)"."VALUES ('".$user_id."', '".$now."');");
				} 
				else 
				{
					// Password incorretta.            
					$now = date('r');
					$link->query("INSERT INTO login_falliti (user_id, orario)"."VALUES ('".$user_id."', '".$now."');");	//Registro il tentativo, l'id dell'utente e l'orario
					header ('Location: ' . 'login.html'); //Reindirizzo alla pagina di login
				}
			}
			else 
			{
				// L'utente inserito non esiste.
				$now = date('r');
				$link->query("INSERT INTO login_falliti (user_id, orario)"."VALUES ('NO_USER', '".$now."');"); //Registro il tentativo fallito e l'orario	
				header ('Location: ' . 'login.html'); //Reindirizzo alla pagina di login
			}
			$stmt->close();
		}
	?>
	</body>
</html>