<?php	
	session_start();
	require 'connessione_db.php';
	if(!isset($_SESSION['username'])&&$_SESSION['username']!='admin')
	{		
		//Verifico che la sessione sia attiva
		header('Location: ' . 'login.html');//Se non attiva reindirizzo alla pagina di login
	}
   	if(isset($_POST['nome'])&&isset($_POST['cognome'])&&isset($_POST['email'])&&isset($_POST['user'])&&isset($_POST['new_password'])&&isset($_POST['confirm_password']))
   	{
    	$password=hash('sha256',$_POST['new_password']);
    	if($stmt=$link->prepare('SELECT username FROM docenti where username="'.$_POST['user'].'";'))
		{
			$stmt->execute();
			$result=$stmt->get_result();
			$stmt->close();
        	if($result->num_rows>0)
            {
            	$esito="<h5>Username già utilizzato</h5>";
         	}
			else
            {
                if($stmt=$link->prepare('SELECT email FROM docenti where email="'.$_POST['email'].'";'))
                {
             	   	$stmt->execute();
					$result=$stmt->get_result();
					$stmt->close();
       			 	if($result->num_rows>0)
            		{
            			$esito="<h5>eMail già utilizzata</h5>";
         			}
            		else{
                			if($stmt=$link->prepare('INSERT INTO docenti(nome, cognome, email, username, password) VALUES ("'.$_POST['nome'].'","'.$_POST['cognome'].'","'.$_POST['email'].'","'.$_POST['user'].'","'.$password.'");'))
							{
								$stmt->execute();
								$esito="<h5>Docente inserito</h5>";
								$stmt->close();
							}
            		}
				}
            }	
   		}
    }
	
?>
<script language="javascript">
	function controlla()
    {
    	var new1=document.inserimento.new_password.value;
    	var new2=document.inserimento.confirm_password.value;
        if(new1==new2)
        {
        	document.getElementById("uguale").innerHTML="<h5>Le password coincidono</h5><hr color=green><br><button class='btn btn-outline-success my-2 my-sm-0' type='submit'>Invia</button>";
        }
        else
        {
        	document.getElementById("uguale").innerHTML="<h5>Le password non coincidono</h5><hr color=red>";
            
        }
    }
</script>

<html>
	<head>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<title>Nuovo docente | DB ASL</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/starter-template.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
			<a class="navbar-brand" href="#">DB ASL</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
					    <a class="nav-link" href="home.php">Home</a>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0" action="logout.php">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
				</form>
			</div>
		</nav>
		<div class="container">
        	<h2>Nuovo docente</h2>
            <br>
        	<?php echo $esito;?>
			<form name="inserimento" method="POST">
				 <div class="form-group">
					<label for="nme">Nome:</label>
					<input type="text" class="form-control" name="nome">
				</div>
                <div class="form-group">
					<label for="nme">Cognome:</label>
					<input type="text" class="form-control" name="cognome">
				</div>
                <div class="form-group">
					<label for="nme">Email:</label>
					<input type="text" class="form-control" name="email">
				</div>
                <div class="form-group">
					<label for="nme">User:</label>
					<input type="text" class="form-control" name="user">
				</div>
				<div class="form-group">
					<label for="cognome">Password:</label>
					<input type="password" class="form-control" name="new_password">
				</div>
				<div class="form-group">
					<label for="cognome">Conferma password:</label>
					<input type="password" class="form-control" name="confirm_password" Onkeyup="controlla()">
				</div>
                <div id="uguale">
                </div>
			</form>
		</div>
        <?php include 'footer.php';?>
	</body>
</html>
