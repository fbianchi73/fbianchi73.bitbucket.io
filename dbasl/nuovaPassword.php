<?php
	require 'connessione_db.php';
	function random($lunghezza=12){
		$caratteri_disponibili ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
		$codice = "";
		for($i = 0; $i<$lunghezza; $i++){
			$codice = $codice.substr($caratteri_disponibili,rand(0,strlen($caratteri_disponibili)-1),1);
		}
		return $codice;
	}
	if(isset($_GET['hash'])){

		$hash=$_GET['hash'];

		$id=substr($hash, 64);
		$password_old=substr($hash, 0, 64);
		$password=random(8); //nuova password di 8 caratteri
		if($stmt=$link->prepare('SELECT id, username, password, email FROM docenti where username="'.$id.'" and password="'.$password_old.'";'))
		{
			$stmt->execute();
			$result=$stmt->get_result();
			$stmt->close();
			if($result->num_rows>0){ 
				while($row = $result->fetch_array(MYSQLI_ASSOC)){

					$email=$row['email'];
					//salvo la nuova password al posto della vecchia (in sha256)
					if($stmt=$link->prepare('UPDATE docenti SET docenti.password="'.hash('sha256',$password).'" WHERE username="'.$id.'" and password="'.$password_old.'";'))
					{
                    	$changed=1;
						$stmt->execute();
						$stmt->close();
					}
					if($changed==1)
                    {
						$header= "From: DB ASL \n";
						$header .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
						$header .= "Content-Transfer-Encoding: 7bit\n\n";
						$subject= "DBASL - Nuova password utente";
						$mess_invio="<html><body>";
						$mess_invio.=" La sua nuova password utente è ".$password."<br/>Puoi cambiarla dal sito una volta effettuato l'accesso";
						$mess_invio.='</body><html>';

						if(@mail($email, $subject, $mess_invio, $header))
                        {
							$esito='La password è stata cambiata con successo. Controlla la tua email.<br /><br />'; 
						}
					}

				}
			}
		}
	}
?>
<html>
	<head>
		<title>Nuova Password | DB ASL</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/starter-template.css" rel="stylesheet">
		<link href="css/footer.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
			<a class="navbar-brand" href="#">DB ASL</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

		</nav>
		<div class="container">
        	<h3><?php echo $esito;?></h3>
		</div>
        
        <?php include'footer.php';?>
	</body>
</html>