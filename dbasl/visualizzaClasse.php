<?php
	session_start();
	require 'connessione_db.php';
	if(!isset($_SESSION['username']))
	{		
		//Verifico che la sessione sia attiva
		header('Location: ' . 'login.html');//Se non attiva reindirizzo alla pagina di login
	}
?> 


<html>
	<head>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<title>Visualizza Classe | DB ASL</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/starter-template.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
			<a class="navbar-brand" href="#">DB ASL</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
					    <a class="nav-link" href="home.php">Home</a>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0" action="logout.php">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
				</form>
			</div>
		</nav>
		<div class="container">
       		<div class="row">
				<div class="col-sm">
                  <?php
                  	  if(!isset($_POST['classe']))
                      {
                      	$classe=$_GET['classe'];
                      }
                      else{$classe=$_POST['classe'];}
                      if ($stmt = $link->prepare('SELECT codice, indirizzo, a_s FROM classi where id='. $classe.';')) 
                      { 
                          $stmt->execute(); // esegue la query appena creata.
                          // estrazione dei risultati
                          $result = $stmt->get_result();
                          // conteggio dei record
                          if ($result->num_rows > 0) {
                              while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                                  echo '<h3 class="font-weight-light font-italic">'.$row['codice'].'&nbsp;'.$row['indirizzo'].'&nbsp;'.$row['a_s'].'</h3><br>';
                              }
                          }
						  $stmt->close();
                      }
                  ?>
                </div>
            </div>
       		<div class="row">
        		<div class="col-sm">
                  <form name="select" method="POST" action="visualizzaCompetenze.php">
                       <select class="form-control" name="sel1">
                          <?php
                              if ($stmt = $link->prepare('SELECT alunni.id, alunni.nome, cognome FROM alunni, classi where alunni.classe=classi.id and alunni.classe='. $classe.' order by cognome;')) 
                              { 
                                  $stmt->execute(); // esegue la query appena creata.
                                  //$stmt->store_result();
                                  //$stmt->bind_result($class_id, $nome, $indirizzo); // recupera il risultato della query e lo memorizza nelle relative variabili.
                                  //$stmt->fetch();
                                  // estrazione dei risultati
                                  $result = $stmt->get_result();
                                  // conteggio dei record
                                  if ($result->num_rows > 0) {
                                      while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                                          echo '<option value='.$row['id'].'>'.$row['nome'].'&nbsp;'.$row['cognome'].'</option>';
                                      }
                                  }
								  $stmt->close();
                              }
                              //$stmt->close();
                          ?>
                      </select>
                      <br>
                      <br>
                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Visualizza competenze</button>
                  </form>
                </div>
                <div class="col-sm">
                  <form name="select" method="POST" action="inserimentoValutazione.php">
                       <select class="form-control" name="sel1">
                          <?php
                              if ($stmt = $link->prepare('SELECT alunni.id, alunni.nome, cognome FROM alunni, classi where alunni.classe=classi.id and alunni.classe='. $classe.' order by cognome;')) 
                              { 
                                  $stmt->execute(); // esegue la query appena creata
                                  $result = $stmt->get_result();
                                  // conteggio dei record
                                  if ($result->num_rows > 0) {
                                      while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                                          echo '<option value='.$row['id'].'>'.$row['nome'].'&nbsp;'.$row['cognome'].'</option>';
                                      }
                                  }
								  $stmt->close();
                              }
                          ?>
                      </select>
                      <br>
                      <br>
                      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Inserisci valutazioni</button>
                  </form>
              	</div>
            </div>
			<form align=right method="POST" action="aggiungiAlunno.php">
				<input type="hidden" name="classe" value="<?php echo $classe;?>"/>
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Aggiungi alunno</button>
			</form>
            <form align=right method="POST" action="eliminazioneAlunno.php">
				<button class="btn btn-danger my-2 my-sm-0" type="submit">Elimina alunno</button>
			</form>
		</div>	
        <?php include 'footer.php';?>
	</body>
</html>