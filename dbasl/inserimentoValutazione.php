<?php
	session_start();
	require 'connessione_db.php';
	if(!isset($_SESSION['username']))
	{		
		//Verifico che la sessione sia attiva
		header('Location: ' . 'login.html');//Se non attiva reindirizzo alla pagina di login
	}
    $alunno=$_POST['sel1'];
?>
<html>
	<head>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<title>Inserisci Valutazioni | DB ASL</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/starter-template.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
			<a class="navbar-brand" href="#">DB ASL</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0" action="logout.php">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
				</form>
			</div>
		</nav>
		<div class="container">
          <h2>Competenze</h2>
          <?php
              if ($stmt = $link->prepare('SELECT alunni.nome, alunni.cognome, classi.codice, classi.indirizzo, classi.id, classi.a_s FROM alunni, classi WHERE classi.id=alunni.classe and alunni.id='.$alunno.';')) 
              { 
                  $stmt->execute(); // esegue la query appena creata.
                  
                     // estrazione dei risultati
                  $result = $stmt->get_result();
                  if ($result->num_rows > 0) {
                      while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                      	  $classe=$row['id'];
                          echo '<h3 class="font-weight-light font-italic">'.$row['nome'].'&nbsp;'.$row['cognome'].'<br>'.$row['codice'].'&nbsp;'.$row['indirizzo'].'&nbsp;'.$row['a_s'].'</h3><br>';
                      }
                  }
              }
          ?>
          <br>
          <form method="POST" action="inserisciValutazione.php">
          		<input type="hidden" name="alunno" value="<?php echo $alunno; ?>"/>
                <table class="table">
                  <thead class="thead-dark">
                      <th scope="col">ID</th><th scope="col">Indicatore</th><th scope="col">Livello</th>
                  </thead>
                  <tbody>
                      <?php
                          if ($stmt = $link->prepare('SELECT id, descrizione
                                                    FROM indicatori 
                                                    ORDER BY id ASC;')) 
                          { 
                              $stmt->execute(); // esegue la query appena creata.
                              // estrazione dei risultati
                              $result = $stmt->get_result();
                              if ($result->num_rows > 0) {
                                  while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                                      echo '<tr><td>'.$row['id'].'
                                      <td>'.$row['descrizione'].
                                      '</td><td><select name='.$row['id'].'>
                                      <option value=1>1</option>
                                      <option value=2>2</option>
                                      <option value=3>3</option>
                                      <option value=4>4</option>
                                      <option value=5>5</option>
                                      </select></td></tr>';
                                  }
                              }
                          }       
                      ?>
                  </tbody>
              </table>
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Inserisci valutazioni</button>
          </form>
          <form action="visualizzaClasse.php" method="POST">
              <input type="hidden" name="classe" value="<?php echo $classe; ?>"/>
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Torna alla classe</button>
          </form>
		</div>
        <?php include 'footer.php';?>
	</body>
</html>