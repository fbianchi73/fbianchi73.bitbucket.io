<?php
	require 'connessione_db.php';
	session_start();
    if(!isset($_SESSION['username']))
    {
    	header('location: login.html');
    }
    if (isset($_POST["invio"])) {
		$percorso = "xls/";
		if (is_uploaded_file($_FILES['file1']['tmp_name'])) {
			if (move_uploaded_file($_FILES['file1']['tmp_name'], $percorso.$_FILES['file1']['name'])) { //controllo che il file sia stato caricato nel percorso indicato
                if($_FILES['file1']['name']=='classe.xlsx')
                {
                	if((isset($_POST["classe"]) and isset($_POST["indirizzo"]) and isset($_POST["a_s"]))) //controllo che tutti i campi siano stati riempiti
					{	
						$classe=strtoupper($_POST["classe"]);
						$indirizzo=strtoupper($_POST["indirizzo"]);	
						$a_s=$_POST["a_s"];
						$classExists=0;
						if($stmt=$link->prepare('SELECT codice, indirizzo, a_s FROM classi;'))
						{
							$stmt->execute(); //eseguo la query appena creata
							$result = $stmt->get_result();
							$stmt->close();
							$exists=0;
							while($row = $result->fetch_array(MYSQLI_NUM)){
								if($row['codice']==$classe && $row['indirizzo']==$indirizzo && $row['a_s']==$a_s) //controllo che la classe non sia già esistente scorrendo i risultati
								{
									echo "La classe ".$classe."&nbsp;".$indirizzo." già esiste nell'anno scolastico".$a_s;
									$exists=1;
								}
							}
							if($exists==0)
							{
                            	$esito="<h3>";
								if($stmt=$link->prepare('INSERT INTO classi (codice, indirizzo, a_s)'.' VALUES ("'.$classe.'", "'.$indirizzo.'", "'.$a_s.'");'))
								{	 
									$stmt->execute();
									if($stmt=$link->prepare('SELECT id FROM classi where codice="'.$classe.'" and indirizzo="'.$indirizzo.'";'))
									{
										$stmt->execute();
										$result = $stmt->get_result();
										$stmt->close();
										if ($result->num_rows > 0) {
											while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
												$id = $row['id']; //assegno alla variabile l'id della classe appena creata
											}
											try {					
													include_once './PHPExcel/IOFactory.php'; //includo la libreria
													$inputFileName = 'xls/classe.xlsx'; //apro il file caricato prima
													$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
													$objReader = PHPExcel_IOFactory::createReader($inputFileType);
													$phpExcel = $objReader->load($inputFileName);
													$sheet = $phpExcel->getSheet(0);
											} catch (Exception $e) { //controllo che non ci siano eccezioni
												echo "ERRORE";
												echo $e->getMessage();
											}
											$counter=0;
											$highestRow = $sheet->getHighestRow(); //calcolo il numero di righe
											$highestColumn = $sheet->getHighestColumn(); //calcolo il numero di colonne
											for ($row = 2; $row <= $highestRow; $row++): //parto dalla riga 2 poiché la precedente contiene il nome dei campi
												$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
												$i=0;$nome=array(); 
												foreach ($rowData[0] as $col): //scorro le colonne
													switch($i)
													{
														case 0: //la prima colonna contiene i nomi
															$name[$counter]=strtoupper($col); //salvo i valori in un array
															break;
														case 1: //la seconda colonna contiene i cognomi
															$surname[$counter]=strtoupper($col); //operazione analoga
															break;
													}
													$i++;
												endforeach;
												$counter++; 
											endfor;
											for ($i=0;$i<$counter;$i++):
												if($stmt=$link->prepare('INSERT INTO alunni (nome, cognome, classe)
												VALUES ("'.$name[$i].'", "'.$surname[$i].'","'.$id.'");'))	//inserisco gli alunni
												{
													$stmt->execute();
													$stmt->close();
													$esito.= "<br>Alunno: "." ". $name[$i] ." ". $surname[$i] . " inserito!<br>";
												} else {
													echo "Error: " . $link->error;
												}
											endfor;
										}    
									}
									$esito.="</h3>";
                                }
                                
							}
						}
					}
				}
                else
                {
                	$errore = 1;
                }
          		unlink("xls/classe.xlsx");//elimino il file precedentemente caricato
			} else {
				echo "si è verificato un errore durante l'upload: ".$_FILES["file1"]["error"];
			}
		}
	}
?>


	<head>
		<title>Aggiungi Classe | DB ASL</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/starter-template.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
			<a class="navbar-brand" href="#">DB ASL</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="index.php">Home</a>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0" action="logout.php">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
				</form>
			</div>
		</nav>
		<div class="container">
			<form enctype="multipart/form-data" name="uploadform" method="POST"> 
				<label for="classe">Classe: </label><input type="text" class="col-2 form-control" name="classe" placeholder="Esempio: 3B" id="classe"/> 
				<label for="indirizzo">Indirizzo: </label><input type="text" class="col-2 form-control" name="indirizzo" placeholder="Esempio: ELETTRONICA" id="indirizzo"/> 
				<label for="anno scolastico">Anno Scolastico: </label><input type="text" class="col-2 form-control" placeholder="Esempio: 2017/18" name="a_s" id="a_s"/> 
				<label for="file1">Seleziona il file classe.xlsx precedentemente scaricato:</label><br><input accept='.xlsx' class="btn btn-outline-success my-2 my-sm-0" type="file" name="file1" size="50"/>
				<input class="btn btn-outline-success my-2 my-sm-0" type="submit" value="Invia" name="invio"/>
			</form>
			
			<a href="download/classe.xlsx">Scarica modello</a>
            <br><br>
            <?php
            	echo $esito;
            	if($errore==1){
     		       	echo '<h5>FILE ERRATO</h5><br>Il file deve essere il modello modificato';
            	}
            ?>
		</div>
        <?php include 'footer.php';?>
	</body>