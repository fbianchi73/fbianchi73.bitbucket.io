-- phpMyAdmin SQL Dump
-- version 4.1.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Giu 14, 2018 alle 14:14
-- Versione del server: 5.6.33-log
-- PHP Version: 5.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bianchif36329`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `alunni`
--

CREATE TABLE IF NOT EXISTS `alunni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(32) NOT NULL,
  `cognome` varchar(32) NOT NULL,
  `classe` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dump dei dati per la tabella `alunni`
--

INSERT INTO `alunni` (`id`, `nome`, `cognome`, `classe`) VALUES
(1, 'PAOLO', 'ROSSI', 1),
(2, 'LUCA', 'NAPOLI', 1),
(3, 'GIULIO', 'FRANCHI', 1),
(4, 'LUISA', 'GIOVANNINI', 1),
(5, 'ALESSANDRA', 'VERDIANI', 1),
(6, 'GIACOMO', 'VECCHIARELLI', 1),
(7, 'FRANCO', 'NATILI', 1),
(8, 'GIANLUCA', 'CRISTIANI', 2),
(9, 'FILIPPO', 'LATINI', 2),
(10, 'GIUSEPPE', 'DE ROSSI', 2),
(11, 'ANNAMARIA', 'VERDI', 2),
(12, 'GIUSEPPE', 'FRANCI', 2),
(13, 'SARA', 'TARDINI', 2),
(14, 'MARCO', 'FASSI', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `classi`
--

CREATE TABLE IF NOT EXISTS `classi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codice` varchar(3) NOT NULL,
  `indirizzo` varchar(32) NOT NULL,
  `a_s` varchar(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `classi`
--

INSERT INTO `classi` (`id`, `codice`, `indirizzo`, `a_s`) VALUES
(1, '3B', 'INFORMATICA', '2017/18'),
(2, '5A', 'ELETTROTECNICA', '2017/18');

-- --------------------------------------------------------

--
-- Struttura della tabella `docenti`
--

CREATE TABLE IF NOT EXISTS `docenti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(32) NOT NULL,
  `cognome` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dump dei dati per la tabella `docenti`
--

INSERT INTO `docenti` (`id`, `nome`, `cognome`, `email`, `username`, `password`) VALUES
(1, 'Admin', '', 'federico.bianchi@ittvt.gov.it', 'admin', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08'),
(2, 'Antonio', 'Rossi', 'federicobianchi14@gmail.com', 'user', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08');

-- --------------------------------------------------------

--
-- Struttura della tabella `indicatori`
--

CREATE TABLE IF NOT EXISTS `indicatori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descrizione` varchar(256) NOT NULL,
  `soggetto` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dump dei dati per la tabella `indicatori`
--

INSERT INTO `indicatori` (`id`, `descrizione`, `soggetto`) VALUES
(1, 'Comunicazione e socializzazione di esperienze e conoscenze', 'A'),
(2, 'Relazione con i formatori e le altre figure adulte', 'AS'),
(3, 'Curiosit&agrave;', 'A'),
(4, 'Superamento delle difficolt&agrave;', 'A'),
(5, 'Rispetto dei tempi', 'A'),
(6, 'Cooperazione e disponibilit&agrave; ad assumersi incarichi e portarli a termine', 'A'),
(7, 'Precisione e destrezza nell''utilizzo degli strumenti e delle tecnologie', 'AS'),
(8, 'Funzionalit&agrave;', 'S'),
(9, 'Uso del linguaggio settoriale - tecnico - professionale', 'S'),
(10, 'Completezza, pertinenza, organizzazione', 'AS'),
(11, 'Capacit&agrave; di trasferire le conoscenze acquisite', 'S'),
(12, 'Ricerca e gestione delle informazioni', 'A'),
(13, 'Consapevolezza riflessiva e critica', 'S'),
(14, 'Autovalutazione', 'S'),
(15, 'Capacit&agrave; di cogliere i processi culturali, scientifici e tecnologici\nsottostanti al lavoro svolto', 'S'),
(16, 'Creativit&agrave;', 'A'),
(17, 'Autonomia', 'A');

-- --------------------------------------------------------

--
-- Struttura della tabella `login_falliti`
--

CREATE TABLE IF NOT EXISTS `login_falliti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(32) NOT NULL,
  `orario` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dump dei dati per la tabella `login_falliti`
--

INSERT INTO `login_falliti` (`id`, `user_id`, `orario`) VALUES
(1, 'NO_USER', 'Thu, 26 Apr 2018 18:57:31 +0000'),
(2, '3', 'Thu, 26 Apr 2018 18:57:47 +0000'),
(3, '3', 'Thu, 26 Apr 2018 18:57:58 +0000'),
(4, 'NO_USER', 'Thu, 10 May 2018 20:53:58 +0200'),
(5, 'NO_USER', 'Thu, 10 May 2018 20:54:21 +0200'),
(6, 'NO_USER', 'Wed, 16 May 2018 13:16:57 +0200'),
(7, 'NO_USER', 'Wed, 30 May 2018 09:08:11 +0200'),
(8, '5', 'Wed, 30 May 2018 12:37:20 +0200'),
(9, '5', 'Thu, 31 May 2018 12:54:28 +0200'),
(10, '4', 'Thu, 31 May 2018 12:56:31 +0200'),
(11, '5', 'Thu, 31 May 2018 12:56:36 +0200');

-- --------------------------------------------------------

--
-- Struttura della tabella `login_riusciti`
--

CREATE TABLE IF NOT EXISTS `login_riusciti` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(32) NOT NULL,
  `orario` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=104 ;

--
-- Dump dei dati per la tabella `login_riusciti`
--

INSERT INTO `login_riusciti` (`id`, `user_id`, `orario`) VALUES
(1, '3', 'Thu, 26 Apr 2018 18:58:25 +0000'),
(2, '3', 'Thu, 26 Apr 2018 19:09:31 +0000'),
(3, '3', 'Mon, 30 Apr 2018 10:39:10 +0000'),
(4, '3', 'Mon, 30 Apr 2018 10:50:51 +0000'),
(5, '3', 'Mon, 30 Apr 2018 11:20:49 +0000'),
(6, '3', 'Tue, 08 May 2018 17:23:57 +0200'),
(7, '3', 'Tue, 08 May 2018 17:24:28 +0200'),
(8, '3', 'Wed, 09 May 2018 08:17:05 +0200'),
(9, '3', 'Wed, 09 May 2018 08:20:29 +0200'),
(10, '3', 'Wed, 09 May 2018 17:30:57 +0200'),
(11, '3', 'Wed, 09 May 2018 17:34:38 +0200'),
(12, '3', 'Wed, 09 May 2018 17:34:59 +0200'),
(13, '3', 'Wed, 09 May 2018 17:37:52 +0200'),
(14, '3', 'Wed, 09 May 2018 17:40:33 +0200'),
(15, '3', 'Wed, 09 May 2018 17:44:19 +0200'),
(16, '3', 'Thu, 10 May 2018 09:27:55 +0200'),
(17, '3', 'Thu, 10 May 2018 10:20:22 +0200'),
(18, '3', 'Thu, 10 May 2018 17:12:31 +0200'),
(19, '3', 'Thu, 10 May 2018 19:18:03 +0200'),
(20, '3', 'Thu, 10 May 2018 20:45:00 +0200'),
(21, '3', 'Thu, 10 May 2018 20:50:54 +0200'),
(22, '3', 'Thu, 10 May 2018 21:00:14 +0200'),
(23, '3', 'Thu, 10 May 2018 21:11:35 +0200'),
(24, '3', 'Fri, 11 May 2018 10:12:47 +0200'),
(25, '3', 'Fri, 11 May 2018 11:07:47 +0200'),
(26, '3', 'Mon, 14 May 2018 09:27:39 +0200'),
(27, '3', 'Tue, 15 May 2018 10:15:50 +0200'),
(28, '3', 'Tue, 15 May 2018 11:02:19 +0200'),
(29, '3', 'Tue, 15 May 2018 11:29:17 +0200'),
(30, '3', 'Tue, 15 May 2018 12:15:20 +0200'),
(31, '3', 'Tue, 15 May 2018 13:23:10 +0200'),
(32, '3', 'Tue, 15 May 2018 15:15:45 +0200'),
(33, '3', 'Tue, 15 May 2018 16:09:02 +0200'),
(34, '3', 'Tue, 15 May 2018 16:11:41 +0200'),
(35, '3', 'Tue, 15 May 2018 16:48:51 +0200'),
(36, '3', 'Tue, 15 May 2018 16:58:04 +0200'),
(37, '3', 'Tue, 15 May 2018 17:16:37 +0200'),
(38, '3', 'Tue, 15 May 2018 17:25:39 +0200'),
(39, '3', 'Tue, 15 May 2018 17:37:53 +0200'),
(40, '3', 'Tue, 15 May 2018 17:40:24 +0200'),
(41, '3', 'Tue, 15 May 2018 17:42:53 +0200'),
(42, '3', 'Tue, 15 May 2018 21:06:55 +0200'),
(43, '3', 'Wed, 16 May 2018 08:19:37 +0200'),
(44, '3', 'Wed, 16 May 2018 08:27:39 +0200'),
(45, '3', 'Wed, 16 May 2018 09:37:21 +0200'),
(46, '3', 'Wed, 16 May 2018 09:59:28 +0200'),
(47, '3', 'Wed, 16 May 2018 10:02:38 +0200'),
(48, '3', 'Wed, 16 May 2018 12:15:48 +0200'),
(49, '3', 'Wed, 16 May 2018 12:32:14 +0200'),
(50, '3', 'Wed, 16 May 2018 13:17:37 +0200'),
(51, '3', 'Wed, 16 May 2018 13:21:19 +0200'),
(52, '3', 'Thu, 17 May 2018 08:24:14 +0200'),
(53, '3', 'Fri, 18 May 2018 10:05:53 +0200'),
(54, '3', 'Fri, 18 May 2018 10:06:14 +0200'),
(55, '3', 'Fri, 18 May 2018 10:06:25 +0200'),
(56, '3', 'Fri, 18 May 2018 10:06:45 +0200'),
(57, '3', 'Fri, 18 May 2018 10:11:54 +0200'),
(58, '3', 'Sun, 20 May 2018 10:11:06 +0200'),
(59, '4', 'Sun, 20 May 2018 10:36:34 +0200'),
(60, '5', 'Sun, 20 May 2018 10:58:43 +0200'),
(61, '5', 'Sun, 20 May 2018 11:22:48 +0200'),
(62, '5', 'Sun, 20 May 2018 11:27:50 +0200'),
(63, '5', 'Mon, 21 May 2018 12:50:38 +0200'),
(64, '5', 'Mon, 21 May 2018 22:28:58 +0200'),
(65, '5', 'Tue, 22 May 2018 11:30:14 +0200'),
(66, '5', 'Wed, 23 May 2018 09:11:39 +0200'),
(67, '5', 'Thu, 24 May 2018 11:05:47 +0200'),
(68, '5', 'Mon, 28 May 2018 10:46:23 +0200'),
(69, '5', 'Mon, 28 May 2018 10:54:23 +0200'),
(70, '5', 'Wed, 30 May 2018 12:11:41 +0200'),
(71, '5', 'Wed, 30 May 2018 12:24:59 +0200'),
(72, '5', 'Wed, 30 May 2018 12:40:54 +0200'),
(73, '4', 'Wed, 30 May 2018 12:45:06 +0200'),
(74, '5', 'Wed, 30 May 2018 13:10:05 +0200'),
(75, '5', 'Wed, 30 May 2018 13:20:22 +0200'),
(76, '5', 'Wed, 30 May 2018 14:03:57 +0200'),
(77, '5', 'Wed, 30 May 2018 15:00:53 +0200'),
(78, '5', 'Wed, 30 May 2018 15:06:41 +0200'),
(79, '5', 'Thu, 31 May 2018 12:19:56 +0200'),
(80, '5', 'Thu, 31 May 2018 12:26:10 +0200'),
(81, '5', 'Thu, 31 May 2018 12:35:13 +0200'),
(82, '5', 'Thu, 31 May 2018 12:54:23 +0200'),
(83, '5', 'Thu, 31 May 2018 12:58:21 +0200'),
(84, '5', 'Thu, 31 May 2018 13:16:00 +0200'),
(85, '5', 'Mon, 04 Jun 2018 11:54:52 +0200'),
(86, '5', 'Tue, 05 Jun 2018 11:41:26 +0200'),
(87, '5', 'Tue, 05 Jun 2018 12:10:36 +0200'),
(88, '5', 'Tue, 05 Jun 2018 12:40:24 +0200'),
(89, '5', 'Wed, 06 Jun 2018 12:43:34 +0200'),
(90, '5', 'Wed, 06 Jun 2018 13:01:06 +0200'),
(91, '5', 'Wed, 06 Jun 2018 21:22:32 +0200'),
(92, '5', 'Thu, 07 Jun 2018 08:57:59 +0200'),
(93, '4', 'Sat, 09 Jun 2018 12:09:52 +0200'),
(94, '1', 'Sat, 09 Jun 2018 12:10:04 +0200'),
(95, '6', 'Sat, 09 Jun 2018 12:14:06 +0200'),
(96, '1', 'Sat, 09 Jun 2018 12:14:53 +0200'),
(97, '4', 'Mon, 11 Jun 2018 11:55:09 +0200'),
(98, '1', 'Mon, 11 Jun 2018 11:55:34 +0200'),
(99, '1', 'Thu, 14 Jun 2018 11:07:09 +0200'),
(100, '1', 'Thu, 14 Jun 2018 11:27:18 +0200'),
(101, '2', 'Thu, 14 Jun 2018 11:51:43 +0200'),
(102, '2', 'Thu, 14 Jun 2018 11:52:07 +0200'),
(103, '1', 'Thu, 14 Jun 2018 11:52:29 +0200');

-- --------------------------------------------------------

--
-- Struttura della tabella `valutazioni`
--

CREATE TABLE IF NOT EXISTS `valutazioni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` date NOT NULL,
  `livello` int(11) NOT NULL,
  `indicatore` int(11) NOT NULL,
  `alunno` int(11) NOT NULL,
  `docente` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dump dei dati per la tabella `valutazioni`
--

INSERT INTO `valutazioni` (`id`, `data`, `livello`, `indicatore`, `alunno`, `docente`) VALUES
(1, '2018-06-14', 1, 1, 3, 1),
(2, '2018-06-14', 2, 2, 3, 1),
(3, '2018-06-14', 3, 3, 3, 1),
(4, '2018-06-14', 4, 4, 3, 1),
(5, '2018-06-14', 3, 5, 3, 1),
(6, '2018-06-14', 5, 6, 3, 1),
(7, '2018-06-14', 2, 7, 3, 1),
(8, '2018-06-14', 4, 8, 3, 1),
(9, '2018-06-14', 3, 9, 3, 1),
(10, '2018-06-14', 3, 10, 3, 1),
(11, '2018-06-14', 2, 11, 3, 1),
(12, '2018-06-14', 5, 12, 3, 1),
(13, '2018-06-14', 2, 13, 3, 1),
(14, '2018-06-14', 3, 14, 3, 1),
(15, '2018-06-14', 1, 15, 3, 1),
(16, '2018-06-14', 3, 16, 3, 1),
(17, '2018-06-14', 2, 17, 3, 1),
(18, '2018-06-14', 3, 1, 8, 1),
(19, '2018-06-14', 2, 2, 8, 1),
(20, '2018-06-14', 1, 3, 8, 1),
(21, '2018-06-14', 3, 4, 8, 1),
(22, '2018-06-14', 1, 5, 8, 1),
(23, '2018-06-14', 4, 6, 8, 1),
(24, '2018-06-14', 5, 7, 8, 1),
(25, '2018-06-14', 4, 8, 8, 1),
(26, '2018-06-14', 2, 9, 8, 1),
(27, '2018-06-14', 3, 10, 8, 1),
(28, '2018-06-14', 3, 11, 8, 1),
(29, '2018-06-14', 1, 12, 8, 1),
(30, '2018-06-14', 2, 13, 8, 1),
(31, '2018-06-14', 4, 14, 8, 1),
(32, '2018-06-14', 1, 15, 8, 1),
(33, '2018-06-14', 3, 16, 8, 1),
(34, '2018-06-14', 4, 17, 8, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
