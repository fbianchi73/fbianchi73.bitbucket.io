<?php
	session_start();
	require 'connessione_db.php';
	if(!isset($_SESSION['username']))
	{		
		//Verifico che la sessione sia attiva
		header('Location: ' . 'login.html');//Se non attiva reindirizzo alla pagina di login
	}
    $alunno=$_POST['alunno'];
       
?>
<html>
	<head>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<title>Visualizza Valutazioni | DB ASL</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/starter-template.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
			<a class="navbar-brand" href="#">DB ASL</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0" action="logout.php">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
				</form>
			</div>
		</nav>
		<div class="container">
        	<h2>Competenze</h2>
        	<?php
	    	   	if ($stmt = $link->prepare('SELECT alunni.nome, alunni.cognome, classi.codice, classi.a_s, classi.indirizzo FROM alunni, classi WHERE classi.id=alunni.classe and alunni.id='.$alunno.';')) 
				{ 
					$stmt->execute(); // esegue la query appena creata.
                   	// estrazione dei risultati
					$result = $stmt->get_result();
					if ($result->num_rows > 0) {
						while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
							echo '<h3 class="font-weight-light font-italic">'.$row['nome'].'&nbsp;'.$row['cognome'].'<br>'.$row['codice'].'&nbsp;'.$row['indirizzo'].'&nbsp;'.$row['a_s'].'</h3><br>';
						}
					}
					$stmt->close();
				}
			?>
        	<br>
        	<table class="table">
          		<thead class="thead-dark">
              		<tr><th scope="col">Indicatore</th><th scope="col">Livello</th><th scope="col">Data</th></tr>
          		</thead>
          		<tbody>
              		<?php
              			if ($stmt = $link->prepare('SELECT indicatori.id, indicatori.descrizione, valutazioni.livello, valutazioni.data 
                    	                            FROM alunni, classi, valutazioni, indicatori 
                          		                    WHERE classi.id=alunni.classe and alunni.id=valutazioni.alunno and indicatori.id=valutazioni.indicatore 
                                	                and alunni.id='.$alunno.'
                                     	            ORDER BY valutazioni.data DESC;')) 
                  		{ 
                     		 $stmt->execute(); // esegue la query appena creata.
                         	// estrazione dei risultati
                      		$result = $stmt->get_result();
                      		if ($result->num_rows > 0) {
                          		while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                          			echo '<tr><td>'.$row['descrizione'].'</td><td>'.$row['livello'].'</td><td>'.$row['data'].'&nbsp;</td></tr>';
                          		}
                      		}
							$stmt->close();
                  		}  
              		?>
				</tbody>
        	</table>
			<form action="visualizzaCompetenze.php" method="POST">
        		<input type="hidden" name="alunno" value="<?php echo $alunno; ?>"/>
            	<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Torna alle competenze</button>
       		</form>
        </div>
        <?php include 'footer.php';?>
	</body>
</html>