<?php
	session_start();
	require 'connessione_db.php';
	if(!isset($_SESSION['username']))
	{		
		//Verifico che la sessione sia attiva
		header('Location: ' . 'login.html');//Se non attiva reindirizzo alla pagina di login
	}
    $alunno=$_POST['sel1'];
    if(!isset($alunno))
    {
    	$alunno=$_POST['alunno'];
    }
    if(!isset($alunno))
    {
    	$alunno=$_GET['alunno'];
    }
       
?>
<html>
	<head>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
		<title>Visualizza Competenze | DB ASL</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link href="css/starter-template.css" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	</head>
	<body>
		<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
			<a class="navbar-brand" href="#">DB ASL</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
					</li>
				</ul>
				<form class="form-inline my-2 my-lg-0" action="logout.php">
					<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</button>
				</form>
			</div>
		</nav>
		<div class="container">
        <h2>Competenze</h2>
        <?php
	       	if ($stmt = $link->prepare('SELECT alunni.nome, alunni.cognome, classi.codice, classi.indirizzo, classi.a_s, classi.id FROM alunni, classi WHERE classi.id=alunni.classe and alunni.id='.$alunno.';')) 
			{ 
				$stmt->execute(); // esegue la query appena creata.
                   // estrazione dei risultati
				$result = $stmt->get_result();
				if ($result->num_rows > 0) {
					while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
						echo '<h3 class="font-weight-light font-italic">'.$row['nome'].'&nbsp;'.$row['cognome'].'<br>'.$row['codice'].'&nbsp;'.$row['indirizzo'].'&nbsp;'.$row['a_s'].'</h3><br>';
                        $classe=$row['id'];
					}
				}
				$stmt->close();
			}
		?>
        <br>
        <table class="table">
          <thead class="thead-dark">
              <tr><th scope="col">Competenza</th><th scope="col">Valutazione</th></tr>
          </thead>
          <tbody>
              <?php
              		if ($stmt = $link->prepare('SELECT distinct indicatori.id, indicatori.descrizione, valutazioni.livello, valutazioni.data 
                                              FROM alunni, classi, valutazioni, indicatori 
                                              WHERE classi.id=alunni.classe and alunni.id=valutazioni.alunno and indicatori.id=valutazioni.indicatore 
                                              and alunni.id='.$alunno.' and  valutazioni.data=(select MAX(valutazioni.data) from valutazioni)
                                              ORDER BY indicatori.id;')) 
                  { 
                      $stmt->execute(); // esegue la query appena creata.
                      $stmt->bind_result($nome,$cognome,$classe,$indirizzo);
                         // estrazione dei risultati
                      $result = $stmt->get_result();
                      if ($result->num_rows > 0) {
                      	  $i=1;
                          while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                          	$valutazioni[$i]=$row['livello'];
                            $i++;
                          }
                      }
					  $stmt->close();
                  }   
                  echo '<tr><td>Imparare ad imparare</td><td>'.round((($valutazioni[2]+$valutazioni[3]+$valutazioni[4]+$valutazioni[6]+$valutazioni[7]+$valutazioni[12]+$valutazioni[14]+$valutazioni[15]+$valutazioni[17])/9),2).'&nbsp;</td></tr>'
                  	   .'<tr><td>Progettare</td><td>'.round((($valutazioni[1]+$valutazioni[3]+$valutazioni[5]+$valutazioni[6]+$valutazioni[7]+$valutazioni[8]+$valutazioni[10]+$valutazioni[12]+$valutazioni[14]+$valutazioni[16])/10),2).'&nbsp;</td></tr>'
                  	   .'<tr><td>Comunicare</td><td>'.round((($valutazioni[1]+$valutazioni[8]+$valutazioni[9]+$valutazioni[10]+$valutazioni[11])/5),2).'&nbsp;</td></tr>'
                  	   .'<tr><td>Collaborare e partecipare</td><td>'.round((($valutazioni[1]+$valutazioni[2]+$valutazioni[4]+$valutazioni[6]+$valutazioni[14])/5),2).'&nbsp;</td></tr>'
                  	   .'<tr><td>Agire in modo autonomo e responsabile</td><td>'.round((($valutazioni[5]+$valutazioni[6]+$valutazioni[13]+$valutazioni[14]+$valutazioni[17])/5),2).'&nbsp;</td></tr>'
                  	   .'<tr><td>Risolvere problemi</td><td>'.round((($valutazioni[1]+$valutazioni[2]+$valutazioni[4]+$valutazioni[6]+$valutazioni[8]+$valutazioni[11]+$valutazioni[13]+$valutazioni[15])/8),2).'&nbsp;</td></tr>'
                  	   .'<tr><td>Individuare collegamenti e relazioni</td><td>'.round((($valutazioni[3]+$valutazioni[10]+$valutazioni[11]+$valutazioni[12]+$valutazioni[13]+$valutazioni[15]+$valutazioni[16])/7),2).'&nbsp;</td></tr>'
                  	   ."<tr><td>Acquisire ed interpretare l'informazione</td><td>".round((($valutazioni[1]+$valutazioni[2]+$valutazioni[3]+$valutazioni[4]+$valutazioni[7]+$valutazioni[9]+$valutazioni[11]+$valutazioni[12]+$valutazioni[13]+$valutazioni[15]+$valutazioni[16])/11),2).'&nbsp;</td></tr>';
                    
              ?>
			</tbody>
        </table>
        <div class="row">
        	<div class="col-sm">
              <form action="visualizzaValutazioni.php" method="POST">
                  <input type="hidden" name="alunno" value="<?php echo $alunno; ?>"/>
                  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Visualizza tutte le valutazioni</button>
              </form>
            </div>
        	<div class="col-sm">
              <form action="visualizzaClasse.php" method="POST">
                  <input type="hidden" name="classe" value="<?php echo $classe; ?>"/>
                  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Torna alla classe</button>
              </form>
            </div>
        </div> 
    </div>
    
        <?php include 'footer.php';?>