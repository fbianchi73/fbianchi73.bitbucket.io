<?php
	session_start(); //Mi assicuro di chiudere la sessione giusta
	session_destroy(); //Elimino la sessione
	header("Location:"."login.html"); //Reindirizzo alla pagina di login
	exit();
?>